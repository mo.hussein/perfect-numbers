package com.perfectnumber.perfectnumberservice;

import com.perfectnumber.perfectnumberservice.models.PerfectNumber;
import com.perfectnumber.perfectnumberservice.services.PerfectNumberService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class PerfectNumberServiceApplicationTests {

	private PerfectNumberService perfectNumberService = new PerfectNumberService();

	@Test
	public void getPerfectNumberTestTrue(){
		long testNumber = 8128;
		PerfectNumber expectedPerfect = new PerfectNumber(testNumber, true);
		PerfectNumber actualPerfect = perfectNumberService.getPerfectNumber(testNumber);
		assertEquals(expectedPerfect.getNumber(), actualPerfect.getNumber());
		assertEquals(expectedPerfect.isPerfect(), actualPerfect.isPerfect());
	}

	@Test
	public void getPerfectNumberTestFalse(){
		long testNumber = 145;
		PerfectNumber expectedPerfect = new PerfectNumber(testNumber, false);
		PerfectNumber actualPerfect = perfectNumberService.getPerfectNumber(testNumber);
		assertEquals(expectedPerfect.getNumber(), actualPerfect.getNumber());
		assertEquals(expectedPerfect.isPerfect(), actualPerfect.isPerfect());
	}

}
