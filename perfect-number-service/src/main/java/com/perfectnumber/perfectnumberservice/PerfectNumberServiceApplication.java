package com.perfectnumber.perfectnumberservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PerfectNumberServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PerfectNumberServiceApplication.class, args);
	}

}
