package com.perfectnumber.perfectnumberconsumer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
public class PerfectNumberConsumerApplication {

	@Bean
	WebClient webClient(){
		return WebClient.create();
	}

	public static void main(String[] args) {
		SpringApplication.run(PerfectNumberConsumerApplication.class, args);
	}

}
