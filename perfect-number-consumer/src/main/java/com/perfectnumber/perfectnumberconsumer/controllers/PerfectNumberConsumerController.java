package com.perfectnumber.perfectnumberconsumer.controllers;

import com.perfectnumber.perfectnumberconsumer.models.PerfectNumber;
import com.perfectnumber.perfectnumberconsumer.models.PerfectNumbers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("perfects")
public class PerfectNumberConsumerController {
    @Autowired
    private WebClient webClient;

    @Value("${perfectNumber.url}")
    String perfectNumberUrl;


        @RequestMapping(value = "/range", method = GET)
        public PerfectNumbers getListOfPerfectNumbers(@RequestParam long start, @RequestParam long end) {
            final List<Mono<PerfectNumber>> perfectNumber = LongStream.range(start, end).mapToObj(
                            index -> webClient
                                    .get()
                                    .uri(perfectNumberUrl + index)
                                    .retrieve()
                                    .bodyToMono(PerfectNumber.class))
                    .collect(Collectors.toList());

                    List<Long> list = Flux
                    .merge(perfectNumber)
                    .flatMap(resp -> {
                        if (resp.isPerfect()) {
                            return Mono.just(resp.getNumber());
                        } else {
                            return Mono.empty();
                        }
                    })
                    .collectList()
                    .block();
            PerfectNumbers perfectNumbers = new PerfectNumbers();
            perfectNumbers.setPerfectNumbers(list);
            return perfectNumbers;
        }

}
