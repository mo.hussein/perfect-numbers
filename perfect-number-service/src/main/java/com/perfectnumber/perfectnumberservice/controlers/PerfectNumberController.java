package com.perfectnumber.perfectnumberservice.controlers;

import com.perfectnumber.perfectnumberservice.models.PerfectNumber;
import com.perfectnumber.perfectnumberservice.services.PerfectNumberService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("perfect")
@Validated
public class PerfectNumberController {
    PerfectNumberService  perfectNumberService = new PerfectNumberService();
    @RequestMapping(value ="/{number}", method = GET)
    public PerfectNumber isPerfectNumber(@Valid @PathVariable @Min(1) long number){
        return perfectNumberService.getPerfectNumber(number);
    }
}
