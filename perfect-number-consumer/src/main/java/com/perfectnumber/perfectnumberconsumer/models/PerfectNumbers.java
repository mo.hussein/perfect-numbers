package com.perfectnumber.perfectnumberconsumer.models;

import java.util.List;

public class PerfectNumbers {
    private List<Long> perfectNumbers;

    public List<Long> getPerfectNumbers() {
        return perfectNumbers;
    }

    public void setPerfectNumbers(List<Long> perfectNumbers) {
        this.perfectNumbers = perfectNumbers;
    }
}
