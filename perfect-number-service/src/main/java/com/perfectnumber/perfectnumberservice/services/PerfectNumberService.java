package com.perfectnumber.perfectnumberservice.services;

import com.perfectnumber.perfectnumberservice.models.PerfectNumber;

public class PerfectNumberService {
    public PerfectNumber getPerfectNumber(long number){
        boolean perfect = false;
        if(number == 1){
            return new PerfectNumber(1, false);
        }
        long maxIterations = number/2;
        long aliquotSum = 1;

        for(long i = 2; i < maxIterations; i++)
        {
            if(number % i == 0){
                aliquotSum += i + (number/i);
                maxIterations = number/i;
            }
        }
        if(aliquotSum == number)
            perfect = true;

        return new PerfectNumber(number, perfect);
    }

}
