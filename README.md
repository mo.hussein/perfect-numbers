# Perfect Numbers

java program that checks if a number is perfect (as described here: http://en.wikipedia.org/wiki/Perfect_number). The program should contain REST API’s to serve the following operations: Check if a given number is perfect Find all perfect numbers bet

### Perfect Number Service
checks if a given number is perfect or not by calling perfect/{number} <br/>
Open API localhost:8081/swagger-ui/index.html

### Perfect Number Consumer
retrieves all perfect numbers between range, by sending asynchronous
call for each number between the range to Perfect Number Service API. <br/>
/perfect/range
this endpoint takes start, and end parameters and returns List of perfect numbers<br/>
Open API localhost:8082/swagger-ui/index.html<br/>
