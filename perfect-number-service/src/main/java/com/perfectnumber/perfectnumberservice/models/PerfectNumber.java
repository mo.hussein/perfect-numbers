package com.perfectnumber.perfectnumberservice.models;

public class PerfectNumber {
    long number;
    boolean perfect;

    public long getNumber() {
        return number;
    }

    public PerfectNumber(long number, boolean perfect) {
        this.number = number;
        this.perfect = perfect;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean isPerfect() {
        return perfect;
    }

    public void setPerfect(boolean perfect) {
        this.perfect = perfect;
    }
}
